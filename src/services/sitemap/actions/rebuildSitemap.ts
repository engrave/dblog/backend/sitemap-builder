import { logger } from "../../../submodules/shared-library/utils";
const XmlSitemap = require('xml-sitemap');
import { getLatestArticles, getBlog } from '../../../submodules/shared-library/services/cache/cache';
import * as moment from 'moment';
import { IArticle } from '../../../submodules/shared-library/interfaces/IArticle';
import * as fs from 'fs';
import { ICategory } from '../../../submodules/shared-library/interfaces/ICategory';

const handleImages = (images: string[]) => {
    if (!images) return null;
    return images.map(i => ({"image:loc": i}));
}

export default async (domain: string) => {

    const file = `/app/sitemaps/${domain}/sitemap.xml`;

    const template =
`<?xml version="1.0" encoding="UTF-8"?>
<urlset 
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" 
    xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
    <url>
        <loc>https://${domain}/</loc>    
    </url>
 </urlset>`;

    let sitemap = new XmlSitemap(template).setHost(`https://${domain}/`);

    sitemap.addOption('image:image', handleImages)

    const blog = await getBlog(domain);

    if(blog.categories.length) {
        blog.categories.forEach((category: ICategory) => {
            sitemap.add(`category/${category.slug}`, {
                priority: 1
            })
        });
    }

    const articles = await getLatestArticles(blog._id, 0, 5000);

    articles.forEach((article: IArticle) => {

        const options: any = {
            lastmod: moment(article.created).toISOString(),
            priority: 0.8,
        }

        if (article.images && article.images instanceof Array) {
            options["image:image"] = article.images;
        }

        try {
            sitemap.add(article.permlink, options)
        } catch (error) {
            logger.error(error);
            // ignore as it is probably because adding the same permlink
        }

    });


    validateSitemapDirectory(domain);

    fs.writeFileSync(file, sitemap.xml);

    return sitemap.xml;

}

const validateSitemapDirectory = (domain: string) => {
    const path = `/app/sitemaps/${domain}`;
    if(! fs.existsSync(path)) {
        fs.mkdirSync(path, {recursive: true});
    }
}
