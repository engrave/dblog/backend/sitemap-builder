import { Request, Response } from 'express';
import { handleResponseError, logger } from '../../../submodules/shared-library';
import { body } from 'express-validator/check';
import sitemap from '../../../services/sitemap/sitemap.service';

const middleware: any[] =  [
    body('domain').isURL()
];

async function handler(req: Request, res: Response) {
    return handleResponseError(async () => {

        const { domain } = req.body;

        const xml = await sitemap.getSitemap(domain);

        logger.info(`Asked for sitemap for: ${domain}`);

        res.header('Content-Type', 'application/xml');
        return res.send( xml );
        
    }, req, res);
}

export default {
    middleware,
    handler
}
