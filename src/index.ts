declare global {
    namespace NodeJS {
        interface Global {
            __rootdir__: string;
        }
    }
}

global.__rootdir__ = '/app/dist';

import app from './app/app';
import { listenOnPort } from './submodules/shared-library/utils/listenOnPort';

listenOnPort(app, 3000);
