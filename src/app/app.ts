import * as express from 'express';
import routes from './app.routes';
import settings from './app.settings';

const Sentry = require('@sentry/node');
import { RewriteFrames, Debug } from '@sentry/integrations';

Sentry.init({
    dsn: 'https://5f5c9a7ff96c453d9867f05b7c1ff4bf@sentry.engrave.dev/8',
    release: `${process.env.npm_package_name}@${process.env.npm_package_version}`,
    integrations: [
        new RewriteFrames({ root: global.__rootdir__ })
    ]});

const app = express();

settings(app);

routes(app);

export default app;
