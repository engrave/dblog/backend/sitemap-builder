import healthApi from "../routes/health/health.routes";
import sitemapApi from "../routes/sitemap/sitemap.routes";
import { endpointLogger } from "../submodules/shared-library/utils/logger";

function routes(app:any) {
    app.use('/health', healthApi);
    app.use('/sitemap', endpointLogger, sitemapApi);
}

export default routes;

